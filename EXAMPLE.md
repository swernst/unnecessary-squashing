- Adding this as an example for an MR commit.
- Another set of changes to test under different conditions.
- Third attempt with newline added to the squash commit message field.
- Fourth attempt to check trailing whitespace.
- Fifth attempt with single newline at the end of the original commit message.
- Sixth change where I'll amend a commit after the MR is created. Here's the amend.
- Seventh change, lifetime of MR before explicit squashing.
  - First change.
  - Second change.
